# Updated file to work with 1000G Phase 3 data!
# Notes: This is designed to be run one annotation at a time
# If you run it like "> python Step2_make_bed_based_annots_Phase3.py HAR"
# it will substitute HAR at the appropriate spot and finish making the 
# annotation.

import os, sys

#----------------------------------------------------------------------
# Set up the list of chromosomes and other folders
#----------------------------------------------------------------------
mainDir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/"
oneKG="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/1000G_EUR_Phase3_plink/"
hapmapdir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/hapmap3_snps/"
LDSCdir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/software/ldsc/"



#----------------------------------------------------------------------
# Make the rest of the annotation files needed for running LDSC
# partitioned heritability. Command via this Wiki: 
# https://github.com/bulik/ldsc/wiki/LD-Score-Estimation-Tutorial
#----------------------------------------------------------------------

resultsDir = mainDir+sys.argv[1]+"/"
print(resultsDir)
os.chdir(resultsDir)

for i in range(1, 23):
	print("Working on annotation: "+sys.argv[1])
	print("Beginning LDScore calculation for chromosome "+str(i))
	os.system("python "+LDSCdir+"ldsc.py \
		--l2 --bfile "+oneKG+"1000G.EUR.QC."+str(i)+" \
		--ld-wind-cm 1 \
		--annot "+resultsDir+sys.argv[1]+"."+str(i)+".annot.gz \
		--out "+resultsDir+sys.argv[1]+"."+str(i)+" \
		--print-snps "+hapmapdir+"hm."+str(i)+".snp")
	print("Done with LDScore calculation for chromosome "+str(i))

print("Done with annotation: "+sys.argv[1]+"!")
print("***********************************")


