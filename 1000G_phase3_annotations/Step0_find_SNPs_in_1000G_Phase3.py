# Run PLINK to extract the SNPs within sets of BED files
# Working with the 1000 Genomes Phase 3 dataset!

import os, sys, glob

plinkDir = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/1000G_EUR_Phase3_plink/"
BEDdir = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/beds/make_annotations/"
outDir = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/"

os.chdir(BEDdir)
annots = glob.glob("*.bed")
print(annots)

for i in annots:
	#annot = i[:-9]
	annot = i[:-4]
	resultDir = outDir+annot
	print(resultDir)
	if os.path.exists(resultDir+"1000G.EUR.QC.1."+annot+".log"):
		print("It looks like the 1000 Genomes Phase 3 SNPs have already been found for this annotation. Moving on!")
	if not os.path.exists(resultDir):
		os.makedirs(resultDir)
		for j in range(1, 23):
			print("Running PLINK to extract "+i+" SNPs from chromosome "+str(j))
			os.system("plink \
				--bfile "+plinkDir+"1000G.EUR.QC."+str(j)+" --make-just-bim --allow-no-sex \
				--extract range "+i+" --out "+resultDir+"/1000G.EUR.QC."+str(j)+"."+annot)
	print("*******All done with "+i+"!")
print("All done finding the SNPs and making .bim files. The next step is to run Step1_make_bed_based_annots_Phase3.R")
