
setwd("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/")
library(tidyverse)


editCNSannotSNPs <- function(chr, SNPdir, base, annot,outDir) {
  message(paste("***Working on chromosome: ",chr))
  CNSfile <- paste("CNS.",chr,".annot.gz", sep = "")
  message(paste("The original annotation file is: ",CNSfile))
  CNSchr <- read.table(gzfile(paste0("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/1000G_Phase3_cell_type_groups/cell_type_group.3.",chr,".annot.gz")), sep="\t", header= TRUE, stringsAsFactors = FALSE)
  #print(head(CNSchr))
  BIMfile <- paste(SNPdir,base,chr,".",annot, ".bim", sep = "")
  message(paste("The  PLINK bim file is: ",BIMfile))
  BIMchr <- read.table(BIMfile, sep="\t", header= FALSE, stringsAsFactors = FALSE)
  message(paste("There are ",nrow(BIMchr)," SNPs in the BIM file."))
  #print(head(BIMchr))
  message("Here's the proof that there ARE matching SNPs")
  slim <- inner_join(CNSchr,BIMchr,by = c("SNP" = "V2")) #checker
  print(head(slim))
  print(nrow(slim))
  #SNPs <- BIMchr$V2
  CNSchr$isANNOT <- if_else(CNSchr$SNP %in% BIMchr$V2, 1, 0)
  output <- CNSchr[,c(1:4,6)]
  #print(head(output))
  #print(summary(output$isANNOT))
  names(output)[names(output)=="isANNOT"] <- annot
  message("The number of SNPs matching this annotation is:")
  table(output[,5])
  gz1 =  gzfile(paste0(outDir,annot,".",chr,".annot.gz"),"w")
  write.table(output,gz1, col.names = TRUE, sep = "\t", quote = FALSE, row.names = FALSE)
  close(gz1)
}

# ------------------------------------------
# Note: need to make a separate function to deal with 
# the chimp promoters, since there are some chr without 
# any SNPs so the code above fails.
# ------------------------------------------
editCNSannotSNPs2 <- function(chr, SNPdir, base, annot,outDir) {
  message(paste("***Working on chromosome: ",chr))
  CNSfile <- paste("CNS.",chr,".annot.gz", sep = "")
  message(paste("The original annotation file is: ",CNSfile))
  CNSchr <- read.table(gzfile(paste0("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/1000G_Phase3_cell_type_groups/cell_type_group.3.",chr,".annot.gz")), sep="\t", header= TRUE, stringsAsFactors = FALSE)
  CNSchr$isANNOT <- 0
  output <- CNSchr[,c(1:4,6)]
  names(output)[names(output)=="isANNOT"] <- annot
  message("The number of SNPs matching this annotation is:")
  table(output[,5])
  gz1 =  gzfile(paste0(outDir,annot,".",chr,".annot.gz"),"w")
  write.table(output,gz1, col.names = TRUE, sep = "\t", quote = FALSE, row.names = FALSE)
  close(gz1)
}




 for (i in c("HSE_12Opcw_active_merged",
            "HSE_12Fpcw_active_merged",
            "HSE_7pcw_active_merged",
            "HSE_8_5pcw_active_merged",
            "HAR",
            "Sweeps",
            "NeanDepleted")) {
   message("#----------------------")
   message("Starting a new annotation!")
   message("#----------------------")
   for (j in 1:22) {
     test = paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/1000G.EUR.QC.",j,".",i, ".bim", sep = "")
     if (file.exists(test)) {
     message(paste("There is a valid .bim file for chromosome ", j, ", making .annot file now."))
     editCNSannotSNPs(j,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""),
                      "1000G.EUR.QC.",
                      i,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""))}
     else {
       message("There was NO valid .bim file for chromosome ", j, ", making an ALL ZEROS .annot file now.")
           editCNSannotSNPs2(j,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""),"1000G.EUR.QC.",i,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""))
     }
   }
 }

for (i in c("E081_active_marks",
            "E073_active_marks",
            "chimp_PFC_enhancers_hg19",
            "chimp_PFC_promoters_hg19",
            "macaque_PFC_enhancers_hg19",
            "macaque_PFC_promoters_hg19")) {
  message("#----------------------")
  message("Starting a new annotation!")
  message("#----------------------")
  for (j in 1:22) {
    test = paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/1000G.EUR.QC.",j,".",i, ".bim", sep = "")
    if (file.exists(test)) {
      message(paste("There is a valid .bim file for chromosome ", j, ", making .annot file now."))
      editCNSannotSNPs(j,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""),
                       "1000G.EUR.QC.",
                       i,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""))} 
    else {
      message("There was NO valid .bim file for chromosome ", j, ", making an ALL ZEROS .annot file now.")
      editCNSannotSNPs2(j,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""),"1000G.EUR.QC.",i,paste("P:/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/",i,"/",sep=""))
    }
  }
}


