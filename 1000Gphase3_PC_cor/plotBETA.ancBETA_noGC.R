##/usr/local/R-3.2.3/bin/R
##Correlate the effect sizes before and after ancestry regression
options(stringsAsFactors=FALSE)
library(GenomicRanges);

##Rdata files containing GWAS summary statistics
rdatafileloc = "/ifs/loni/faculty/dhibar/ENIGMA3/MA6/evolution/1000Gphase3_PC_cor/AncestryRegressionData_noGC/";
##read in gwas statistics file (compiled for all traits)
fGWASsumstats = "/ifs/loni/faculty/dhibar/ENIGMA3/MA6/evolution/1000Gphase3_PC_cor/AncestryRegressionData_noGC/GWASfiles.txt";
##LDSC intercept values from original ENIGMA (not ancestry regressed)
fLDSCint = "/ifs/loni/faculty/dhibar/ENIGMA3/MA6/evolution/SDS_E3ancreg1KGP3_noGC/code/MA6_LDSC_intercepts_before_after_Phase3_ancreg_Rdata_based_noGC.csv";

##Match the Rdata file locations of sumstats, text file sumstats, and clumped files
GWASsumstats=read.table(fGWASsumstats, header=FALSE)$V1;
##Parse to get trait name
tmpname = sapply(GWASsumstats,function (x) {unlist(strsplit(x,"/",fixed=TRUE))[11]});
tmpnamepheno = paste(sapply(tmpname,function (x) {unlist(strsplit(x,"_",fixed=TRUE))[1]}),sapply(tmpname,function (x) {unlist(strsplit(x,"_",fixed=TRUE))[2]}),sapply(tmpname,function (x) {unlist(strsplit(x,"_",fixed=TRUE))[3]}),sep="_");
phenoname = tmpnamepheno;

##Make output matrices for the correlations
output = matrix(NA,nrow=length(phenoname),ncol=3);
rownames(output) = phenoname;
colnames(output) = c("corBETA","corSE","corlog10P");

##Perform correlations pre and post ancestry regression
for (i in 1:length(GWASsumstats)) {
    load(GWASsumstats[i]);
    output[i,1] = cor(mergedGR$BETA,mergedGR$ancBETA);
    output[i,2] = cor(mergedGR$SE,mergedGR$ancSE);
    output[i,3] = cor(-log10(mergedGR$P),-log10(mergedGR$ancP));
}

write.csv(output,file="BETA.ancBETA.csv",quote=FALSE);
##output = read.csv('BETA.ancBETA.csv',row.names=1);
stop();

##Plot the results
pdf("BETA.ancBETA_noGC.pdf");
plot(1:70,output[,1],ylab="cor(BETA,ancestry adj BETA)",xlab="GWAS",pch=19,xaxt="n",main="Effect sizes");
axis(1,at=1:70,labels=phenoname,las=2,cex.axis=0.3);
plot(output[,2],ylab="cor(SE,ancestry adj SE)",xlab="GWAS",pch=19,xaxt="n",main="Standard Errors");
axis(1,at=1:70,labels=phenoname,las=2,cex.axis=0.3);
plot(output[,2],ylab="cor(-log10(P),ancestry adj -log10(P))",xlab="GWAS",pch=19,xaxt="n",main="P-values");
axis(1,at=1:70,labels=phenoname,las=2,cex.axis=0.3);

##Correlate these to the LDSC intercept values
##Take only the LDSC intercepts from ancestry regressed, non-GC corrected, surface area
LDSCint = read.csv(fLDSCint);
ind = which(LDSCint$Surf_Thic=="Surface Area" & LDSCint$Anc_reg==TRUE);
LDSCint = LDSCint[ind,];
##Rearrange in same order
LDSCint = LDSCint[c(7,1:6,8:35),];

matchedout = output[c(grep("Full_SurfArea",rownames(output)),grep("surfavg",rownames(output))),];

tmpcor = cor.test(matchedout$corBETA,LDSCint$LDSC_intercept);
corval = tmpcor$estimate;
pval = tmpcor$p.value;
model = lm(LDSCint$LDSC_intercept ~ matchedout$corBETA);
plot(matchedout$corBETA,LDSCint$LDSC_intercept,xlab="cor(BETA, ancestry adj BETA)",ylab="LDSC Intercept unadjusted",main=paste0("LDSC intercept vs BETA correlation\ncor=",signif(corval,3),"; pval=",signif(pval,3)),pch=19);
x = range(matchedout$corBETA);
y = model$coefficients[1] + model$coefficients[2]*x;
lines(x,y,lty=2,col="grey");
##Label some of the big ones with text
labelind = which(LDSCint$LDSC_intercept>1.02 | matchedout$corBETA<0.9980);
text(matchedout$corBETA[labelind],LDSCint$LDSC_intercept[labelind],rownames(matchedout)[labelind],pos=4);

dev.off();
