#Ancestry Regression

These scripts detail the implementation of Ancestry Regression on GWAS summary statistics derived from the ENIGMA3 meta-analysis. Ancestry regression is derived from the work of the Alkes Price lab (https://www.biorxiv.org/content/10.1101/076133v1). We implemented ancestry regression in order to reduce the impact of subtle population stratification on GWAS summary statistics, which may confound estimates of selection impacting any trait (https://www.ncbi.nlm.nih.gov/pubmed/30895923).

##Non-GC summary statistics

We first gathered summary statistics that were not subject to genomic control (GC) correction. Those summary statistics look like this:

   	  SNP A1 A2 FREQ1 BETA SE P N MARKER CHR BP
  	  rs2977670 c g 0.9608 758.3807 485.5590 0.1183 10332 1:723891 1 723891
   	  rs143225517 t c 0.8454 718.8055 232.3162 0.001974 15846 1:751756 1 751756
   	  rs146277091 a g 0.0344 -1186.6514 501.2970 0.01793 10501 1:752478 1 752478

We then made a file containing the locations of each GWAS representing different phenotypes (brain regions) that GWAS was run on. This is stored in `GWASfiles_noGC.txt`. In order to get access to these files for ENIGMA, please visit the ENIGMA website: http://enigma.ini.usc.edu.

##Assessing the impact of population stratification on GWAS summary statistics prior to ancestry regression

We first wanted to determine how much population stratification impacted the results of the GWAS summary statistics. One way of assessing this is to correlate principal components describing allele frequency differences across human populations (ancestry PCs) with genome-wide association summary statistics from the trait of interest. Under the assumption that allele frequency differences across human populations would unlikely be driven strongly by selection impacting whatever trait you are studying, but instead by drift and migration, there should be a non-significant correlation of ancestry PCs with GWAS summary statistics. Here's how we evaluated this:

First, we used unrelated individuals from all ancestries (found in `1kg_phase1_all.fam`) from 1000G phase 3 dataset (http://www.internationalgenome.org/data). In this dataset, we calculated ancestry PCs, excluding regions of large LD defined in `high-ld.txt`. Then ancestry PCs were calculated using `PCloading_analysis.py`. This resulted in a file that contains the first 20 ancestry PCs (called `1kg_phase3_ns.allpop.unrel2261_eigenvec.P1to20_beta_se_pval.txt`), and looks like this:

       CHR SNP VAL POS A1 A2 P1beta P1se P1pval P2beta P2se P2pval P3beta P3se P3pval P4beta P4se P4pval P5beta P5se P5pval P6beta P6se P6pval P7beta P7se P7pval P8beta P8se P8pval P9beta P9se P9pval P10beta P10se P10pval P11beta P11se P11pval P12beta P12se
       1       rs575272151     0       11008   G       C       0.007518 0.001082 4.846e-12     0.003671 0.001091 0.000777      0.001789 0.001092 0.1015        -0.003476 0.001091 0.001466     -0.002038 0.001093 0.06231      -0.0002209 0.001093 0.8398      0.001
       1       rs544419019     0       11012   G       C       0.007518 0.001082 4.846e-12     0.003671 0.001091 0.000777      0.001789 0.001092 0.1015        -0.003476 0.001091 0.001466     -0.002038 0.001093 0.06231      -0.0002209 0.001093 0.8398      0.001
       1       rs62635286      0       13116   G       T       -0.008016 0.001009 3.127e-15    0.01301 0.0009862 2.465e-38     -0.001185 0.001022 0.2463       -0.002098 0.001022 0.0403       0.001928 0.001023 0.05954       -0.0006274 0.001023 0.5396      0.001

Then we calculated the correlation between GWAS summary statistics and the ancestry PCs using the following codes. `1000G_PC_cor_BJK_noGC_master.R` which then calls `1000G_PC_cor_BJK_noGC_slave.R`. This is set up to run on our grid in parallel as these correlations are time consuming. After this, the results are plotted as bar plots using `plot1000G_PC_cor_noGC_BJK.R`.

##Correcting for subtle population stratification using ancestry regression

Using a standard multivariable regression implemented in R with the `lm()` function, we regress the GWAS summary statistics prior to ancestry regression (Beta_strat) with the ancestry PCs (Beta_PCs). The residuals of this model are then ancestry corrected effect sizes (Beta_r) and also have ancestry corrected standard errors and P-values. This is implemented in `AncestryRegression_noGC.R`.

##Evaluating the impact of ancestry regression

In order to determine what impact the ancestry regression had, we correlated the effect sizes after ancestry regression (Beta_r) with the ancestry PCs (Beta_PCs). This was done using `1000G_PC_cor_ancreg_BJK_noGC_master.R` which then calls `1000G_PC_cor_ancreg_BJK_noGC_slave.R`. Results were plotted with `plot1000G_PC_cor_ancreg_noGC_BJK.R`.

##Determining how much ancestry regression impacts summary stats

To evaluate how much ancestry regression impacted summary statistics, we correlated summary statistics before and after ancestry regression. We also related this to LDSC intercepts prior to ancestry regression. This is implemented in `plotBETA.ancBETA_noGC_BJK.R`.

