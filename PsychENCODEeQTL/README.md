#Annotating evolutionarily relevant genes that impact brain structure

To further understand genes regulated impacted by common variation within HGEs, we established which of the genome-wide significant SA loci (p-value < 5x10-8 including SNPs in LD at r2 > 0.6) fall within HGEs and also impact gene expression in adult cortical tissue from the PsychENCODE study (expression quantitative trait loci - eQTLs - at FDR < 0.05).

##PsychENCODE data download

Adult brain eQTL data were downloaded from PsychENCODE (http://adult.psychencode.org/) using this dataset: "Set with FDR<0.05 and a filter requiring genes to have an expression > 0.1 FPKM in at least 10 samples".

##Parsing eQTL data

Gene sets impacted by genetic variation within 7 PCW HGEs were derived separately for (1) global SA or (2) any of the 34 regional SA loci. We first identified all SNPs within 10,000 kb of a nominally significant (p-value < 5x10-8) GWAS locus with r2 > 0.6 in the 1000G EUR population to the index SNP, using PLINK 1.9. With this extended list of SNPs in LD with the GWAS index SNP, we looked for overlaps with any of 7 PCW HGEs. For those genome-wide significant loci that also overlapped with HGEs, we then recorded known functional impacts on gene expression using adult brain eQTL from the PsychENCODE dataset. Gene biotype annotations (e.g. protein coding) were called using ENSEMBL via biomaRt. This was implemented in `GO_Enrichment.R`.
     
##Performing Gene ontology

Pathway enrichment was performed for each gene list using the gost function from the ‘gprofiler2’ package (version 0.1.3). Electronic GO annotations (evidence code IEA) were excluded, the sources were limited to GO, KEGG, and Reactome pathways, and FDR correction was applied with a significance threshold of 0.05. This was implemented in `run_gprofileR2.R`.

